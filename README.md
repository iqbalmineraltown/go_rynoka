# Go Rynoka

Learning Go REST API
Based on https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql


## Setup
- Install Postgres & create user each for production and test
- Create Table with the following query
```
CREATE TABLE products
(
    id SERIAL,
    name TEXT NOT NULL,
    price NUMERIC(10,2) NOT NULL DEFAULT 0.00,
    CONSTRAINT products_pkey PRIMARY KEY (id)
)
```
- Set following env var with Postgres credentials
  - `APP_DB_USERNAME`
  - `APP_DB_PASSWORD`
  - `APP_DB_NAME`
  - `TEST_DB_USERNAME`
  - `TEST_DB_PASSWORD`
  - `TEST_DB_NAME`
- Clone this repo then install following dependencies
```
go get github.com/gorilla/mux github.com/lib/pq
```

## Test
 - Windows: `go run .\main.go .\app.go .\model.go ...` (or use `go build; .\go_rynoka.exe` to build and run)
 - Ubuntu: `go run *.go`

 Then you can request to `localhost:8000` using cURL/Insomnia/Postman
 - You can import json file included in this repo into Insomnia
